import csv
import numpy as np
from operator import itemgetter, attrgetter, methodcaller
from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans

def finePatterns(rawPattern,t=0):  # denoise pattern
    _threshold = t
    nPatterns = len(rawPattern)
    bins = [[] for x in range(nPatterns)]
    for j in range(nPatterns):
        for i in range(len(rawPattern[j])):
            if rawPattern[j][i] > _threshold: 
                bins[j].append(i) 
            # bins[rawPattern[i]].append(i)
    return bins
def countSort(labels ,nPatterns): # return the # of samples assigned to each cluster
    bins = [ 0 for x in range(nPatterns)]
    for x in labels :
        bins[x] += 1
    return bins

def isMultiTransaction(row): # for transaction with more than one items, return True
    if row[0] == 'item_0':
        return False
    else:
        cnt = 0
        for x in row:
            if x == '1':
                cnt += 1
        return True if cnt > 1 else False

if __name__ == '__main__':
    # filename = './testdata.csv'
    ''' preprocess data: read file and remove single purcharse trans'''
    filename = './data.csv' # put datafille under same folder
    # filename = './simpledata.csv'

    cnt = 0
    with open(filename, 'r') as csvfile:
        content = csv.reader(csvfile, delimiter = ',')
        rawData = []
        for row in content:
            if isMultiTransaction(row[1:]):
                rawData.append(row[1:])

    X = np.asarray(rawData)
    del rawData
    ''' apply kmeans learning''' # use kmeans learning to find out the `average` patterns of purchases including more than one item
    nTrnsc, nItem = X.shape


    nPatterns = 10 # assume the no. of patterns (not cluster centers), default = 10
    nRuns = 5 # run Kmeans nRuns times and calculate the average, default =5
    confidence = 0.4 # confidence for learned patterns to include one item
    clf = MiniBatchKMeans(n_clusters = nPatterns, n_init = nRuns)
    labels = clf.fit_predict(X)
    patternCnt = countSort(labels,nPatterns)
    rawPatterns = clf.cluster_centers_ 

    patterns = finePatterns(rawPatterns,confidence)
    # print finePatterns

    patternCntByIndex = [(patternCnt[i], i) for i in range(len(patternCnt))]
    # pattern Counnt By Index order
    # print patternCntByIndex
    sortedPattern = sorted(patternCntByIndex, key=itemgetter(0), reverse=True) # PCI for patternCntByIndex; sort the patterns by pattern count
    # print sortedPattern

    '''filter frequency'''
    freq = 0.4 # output pattern over this frequency
    sortPI = map(lambda x: x[1], filter(lambda x: x[0] > freq*nItem, sortedPattern)) # filter patterns with large freqency (here freq= 0.2)
    sortPatterns = map(patterns.__getitem__,sortPI)
    sortPatterns = filter(lambda x: len(x)> 1, sortPatterns)
    print sortPatterns
    