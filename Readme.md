## McKinsey Digtial Lab interview Excersise
@pennlio pennlio@gmail.com
-----------

> the whole project can be found at [https://bitbucket.org/pennlio/mdl-data-test]

-----------
Exercise: Given a dataset of transactions (100k rows) from a large retailer, find groups of items that are frequently purchased together. Each row is a single transaction with a '1' denoting that item was in the transaction. Submit your findings along with any code/description of tools used to solve the problem.

The dataset can be found at [http://bit.ly/1sCk2vd]


Methods
---------
- K-means learn the serveral 'average' patterns behind the data and it `averages out` the noises, so I consider K-means to unsuperivesely learn custormers' purchasing patterns across these items


Default Parmater setting
----------
- nPatterns = 10 
- nRuns = 5
- confidence = 0.4
- freq = 0.4


Output:
---------
- several combinations of items that are frequently bought by customers in descending order (for the default setting):

[3, 5, 22]
[2, 7, 29]
[1, 9, 35, 39, 42]
[2, 7]
[2, 29]
[1, 9, 35, 39]
[2, 7, 29, 35]
[5, 14, 32]

- due to the different understanding of 'frequent', one can adjust the parameters to generate more/less tight solutions.


