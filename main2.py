import csv
import numpy as np
from operator import itemgetter, attrgetter, methodcaller
from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans
# import matplotlib.pyplot as plt

def finePatterns(rawPattern,t=0):  # denoise pattern
    _threshold = t
    nClusters = len(rawPattern)
    bins = [[] for x in range(nClusters)]
    for j in range(nClusters):
        for i in range(len(rawPattern[j])):
            if rawPattern[j][i] > _threshold: 
                bins[j].append(i) 
            # bins[rawPattern[i]].append(i)
    return bins
# print rawData[1:3,:]
def countSort(labels ,nClusters): # return the # of samples assigned to each cluster
    bins = [ 0 for x in range(nClusters)]
    for x in labels :
        bins[x] += 1
    return bins

def isMultiTransaction(row): # for transaction with more than one items, return True
    if row[0] == 'item_0':
        return False
    else:
        cnt = 0
        for x in row:
            if x == '1':
                cnt += 1
        return True if cnt > 1 else False

if __name__ == '__main__':
    filename = './testdata.csv'
    # filename = './data.csv'
    # filename = './simpledata.csv'
    
    ''' preprocess data: read file and remove single purcharse trans'''
    cnt = 0
    with open(filename, 'r') as csvfile:
        content = csv.reader(csvfile, delimiter = ',')
        rawData = []
        for row in content:
            if isMultiTransaction(row[1:]):
                rawData.append(row[1:])

    X = np.asarray(rawData)

    ''' apply kmeans learning''' # use kmeans learning to find out the `average` patterns of purchases including more than one item
    n_features, nSamples = X.shape
    nClusters = 10 # assume the no. of patterns (cluster centers), default = 10
    nRuns = 5 # run Kmeans nRuns times and calculate the average
    confidence = 0.4 # confidence for learned patterns to include one item
    clf = MiniBatchKMeans(n_clusters = nClusters, n_init = nRuns)
    labels = clf.fit_predict(X)
    patternCnt = countSort(labels,nClusters)
    rawPatterns = clf.cluster_centers_
        # print rawPatterns
    #     if clf.inertia_ < minInertia:
    #         minInertia = clf.inertia_
    #         minN_cluster = nClusters
    #         minclf = clf
    #         minrawPatterns = rawPatterns
    
    patterns = finePatterns(rawPatterns,confidence)
    # print sorted(patternCnt, reverse=True)
    # print finePatterns
    patternCntByIndex = [(patternCnt[i], i) for i in range(len(patternCnt))]
    # pattern Counnt By Index order
    # print patternCntByIndex

    sortedPattern = sorted(patternCntByIndex, key=itemgetter(0), reverse=True) # PCI for patternCntByIndex; sort the patterns by pattern count
    # print sortedPattern

    '''filter frequency'''
    freq = 0.2
    sortPI = map(lambda x: x[1], filter(lambda x: x[0] > freq*nSamples, sortedPattern)) # filter patterns with large freqency (here freq= 0.2)
    sortPatterns = map(patterns.__getitem__,sortPI)
    print sortPatterns
    
    # '''draw figure'''
    # bar = [0]*n_features






    # print patternCnt # count for each pattern
    # print sum(pattern_cnt)
    # print bins