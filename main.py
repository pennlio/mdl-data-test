import csv
import numpy as np
from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans

def clusterItems(y, n_clusters):
    bins = [[] for x in range(n_clusters)]
    for i in range(len(y)):
        bins[y[i]].append(i)
    return bins
# print rawData[1:3,:]

if __name__ == '__main__':
    filename = './testdata.csv'
    # filename = './data.csv'
    # filename = './simpledata.csv'
    cnt = 0
    with open(filename, 'r') as csvfile:
        content = csv.reader(csvfile, delimiter = ',')
        rawData = []
        for row in content:
            if cnt:rawData.append(row[1:])
            else: cnt = 1
    rawData = np.asarray(rawData)
    X = rawData.transpose()
    ''' kmeans learning'''
    # print rawData.shape  # verify right
    n_features, nsamples = X.shape

    minInertia = float('inf')
    for n_clusters in range(10,11):
        clf = MiniBatchKMeans(n_clusters = n_clusters)
        y = clf.fit_predict(X)
        if clf.inertia_ < minInertia:
            minInertia = clf.inertia_
            minN_cluster = n_clusters
            minclf = clf
            minY = y
    # print minY
    bins = clusterItems(minY, minN_cluster)
    print bins